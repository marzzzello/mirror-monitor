import unittest
import responses
import tempfile

from fdroid_mirror_monitor import repo, mirror, utils


class TestMethods(unittest.TestCase):
    url = 'https://main.local/repo/'
    integrated_mirror_url = 'https://integrated_mirror.local/repo/'
    additional_mirror_url = 'https://additional_mirror.local/repo/'
    try:
        log = utils.get_logger(__name__, 'DEBUG')
    except RuntimeError:
        log = utils.get_logger(__name__)

    index = {
        'repo': {
            'timestamp': 1572620793000,
            'version': 19,
            'name': 'microG F-Droid repo',
            'icon': 'fdroid-icon.png',
            'address': 'https://microg.org/fdroid/repo',
            'description': 'This is a repository of microG apps to be used with F-Droid. Applications in this repository are signed official binaries built by the microG Team from the corresponding source code. ',
            'pubkey': '308202ed308201d5a003020102020426ffa009300d06092a864886f70d01010b05003027310b300906035504061302444531183016060355040a130f4e4f47415050532050726f6a656374301e170d3132313030363132303533325a170d3337303933303132303533325a3027310b300906035504061302444531183016060355040a130f4e4f47415050532050726f6a65637430820122300d06092a864886f70d01010105000382010f003082010a02820101009a8d2a5336b0eaaad89ce447828c7753b157459b79e3215dc962ca48f58c2cd7650df67d2dd7bda0880c682791f32b35c504e43e77b43c3e4e541f86e35a8293a54fb46e6b16af54d3a4eda458f1a7c8bc1b7479861ca7043337180e40079d9cdccb7e051ada9b6c88c9ec635541e2ebf0842521c3024c826f6fd6db6fd117c74e859d5af4db04448965ab5469b71ce719939a06ef30580f50febf96c474a7d265bb63f86a822ff7b643de6b76e966a18553c2858416cf3309dd24278374bdd82b4404ef6f7f122cec93859351fc6e5ea947e3ceb9d67374fe970e593e5cd05c905e1d24f5a5484f4aadef766e498adf64f7cf04bddd602ae8137b6eea40722d0203010001a321301f301d0603551d0e04160414110b7aa9ebc840b20399f69a431f4dba6ac42a64300d06092a864886f70d01010b0500038201010007c32ad893349cf86952fb5a49cfdc9b13f5e3c800aece77b2e7e0e9c83e34052f140f357ec7e6f4b432dc1ed542218a14835acd2df2deea7efd3fd5e8f1c34e1fb39ec6a427c6e6f4178b609b369040ac1f8844b789f3694dc640de06e44b247afed11637173f36f5886170fafd74954049858c6096308fc93c1bc4dd5685fa7a1f982a422f2a3b36baa8c9500474cf2af91c39cbec1bc898d10194d368aa5e91f1137ec115087c31962d8f76cd120d28c249cf76f4c70f5baa08c70a7234ce4123be080cee789477401965cfe537b924ef36747e8caca62dfefdd1a6288dcb1c4fd2aaa6131a7ad254e9742022cfd597d2ca5c660ce9e41ff537e5a4041e37',
            'fingerprint': '9BD06727E62796C0130EB6DAB39B73157451582CBD138E86C468ACC395D14165',
        },
        'requests': {'install': [], 'uninstall': []},
        'apps': [
            {
                'categories': ['System'],
                'changelog': 'https://github.com/microg/android_packages_apps_FakeStore/releases',
                'suggestedVersionCode': '16',
                'description': '<p>An empty package that mocks the existence of the Google Play Store to the Play Services client.</p>',
                'issueTracker': 'https://github.com/microg/android_packages_apps_FakeStore/issues',
                'license': 'Apache2',
                'name': 'FakeStore',
                'sourceCode': 'https://github.com/microg/android_packages_apps_FakeStore',
                'summary': 'microG Store (FakeStore release)',
                'webSite': 'https://microg.org/',
                'added': 1443985828000,
                'icon': 'com.android.vending.16.png',
                'packageName': 'com.android.vending',
                'lastUpdated': 1479263624000,
            },
            {
                'antiFeatures': ['NonFreeNet', 'UpstreamNonFree'],
                'categories': ['System'],
                'changelog': 'https://github.com/microg/android_packages_apps_RemoteDroidGuard/commits/master',
                'suggestedVersionCode': '4',
                'description': "<p>Service to run Google's DroidGuard binary in an isolated environment.</p><p>Contains and downloads proprietary Google code to your device. Only required if you want to use applications that require SafetyNet or to test DroidGuard.</p>",
                'issueTracker': 'https://github.com/microg/android_packages_apps_RemoteDroidGuard/issues',
                'license': 'Proprietary',
                'name': 'microG DroidGuard Helper',
                'sourceCode': 'https://github.com/microg/android_packages_apps_RemoteDroidGuard',
                'summary': 'microG DroidGuard Helper',
                'webSite': 'https://github.com/microg/android_packages_apps_RemoteDroidGuard',
                'added': 1474766212000,
                'icon': 'org.microg.gms.droidguard.14.png',
                'packageName': 'org.microg.gms.droidguard',
                'lastUpdated': 1495244777000,
            },
            {
                'antiFeatures': ['NonFreeNet'],
                'categories': ['System'],
                'changelog': 'https://github.com/microg/android_packages_apps_GmsCore/commits/master',
                'suggestedVersionCode': '19420020',
                'description': '<p>microG GmsCore is a FLOSS (Free/Libre Open Source Software) framework to allow applications designed for Google Play Services to run on systems, where Play Services is not available.</p><p>As the original Play Services, microG GmsCore allows you to contact certain Google services. These services are not open-source.</p>',
                'issueTracker': 'https://github.com/microg/android_packages_apps_GmsCore/issues',
                'license': 'Apache2',
                'name': 'microG Services Core',
                'sourceCode': 'https://github.com/microg/android_packages_apps_GmsCore',
                'summary': 'Re-Implementation of Play Services',
                'webSite': 'https://microg.org/',
                'added': 1453922398000,
                'icon': 'com.google.android.gms.19420020.png',
                'packageName': 'com.google.android.gms',
                'lastUpdated': 1572597857000,
            },
            {
                'antiFeatures': ['NonFreeNet'],
                'categories': ['System'],
                'changelog': 'https://github.com/microg/android_packages_apps_GsfProxy/commits/master',
                'suggestedVersionCode': '8',
                'description': '<p>This is needed by some older client libraries for cloud-to-device-messaging (C2DM), nowadays called Cloud Messaging (GCM).</p><p>It will redirect the request to GmsCore and thus will not work stand-alone.</p>',
                'issueTracker': 'https://github.com/microg/android_packages_apps_GsfProxy/issues',
                'license': 'Apache2',
                'name': 'microG Services Framework Proxy',
                'sourceCode': 'https://github.com/microg/android_packages_apps_GsfProxy',
                'summary': 'Legacy support proxy library',
                'webSite': 'https://microg.org/',
                'added': 1454843696000,
                'icon': 'com.google.android.gsf.8.png',
                'packageName': 'com.google.android.gsf',
                'lastUpdated': 1454843696000,
            },
        ],
        'packages': {
            'com.android.vending': [
                {
                    'added': 1479263624000,
                    'apkName': 'com.android.vending-16.apk',
                    'hash': '7fbf38e5a6f7958442547971eb0fcdcbe6429ff1db8141e048516eaa65e48769',
                    'hashType': 'sha256',
                    'minSdkVersion': '9',
                    'packageName': 'com.android.vending',
                    'sig': 'ca625df9dd76b6002de9c94d216cf09b',
                    'signer': '9bd06727e62796c0130eb6dab39b73157451582cbd138e86c468acc395d14165',
                    'size': 13133,
                    'targetSdkVersion': '24',
                    'uses-permission': [['android.permission.FAKE_PACKAGE_SIGNATURE', None]],
                    'versionCode': 16,
                    'versionName': '0.0.2',
                },
                {
                    'added': 1443985828000,
                    'apkName': 'com.android.vending-1.apk',
                    'hash': 'c34407656d196e281d07058d05f7d046a0efafcda5a9e3cc241149f55c2a83a6',
                    'hashType': 'sha256',
                    'minSdkVersion': '10',
                    'packageName': 'com.android.vending',
                    'sig': 'ca625df9dd76b6002de9c94d216cf09b',
                    'signer': '9bd06727e62796c0130eb6dab39b73157451582cbd138e86c468acc395d14165',
                    'size': 7958,
                    'targetSdkVersion': '23',
                    'uses-permission': [['android.permission.FAKE_PACKAGE_SIGNATURE', None]],
                    'versionCode': 1,
                    'versionName': '0.0.1',
                },
            ],
            'com.google.android.gms': [
                {
                    'added': 1572597857000,
                    'apkName': 'com.google.android.gms-19420020.apk',
                    'hash': 'd4e1dc438436b55d4b9738d161e52003a24535c1fd18e3a7f8b09cb0ba35c320',
                    'hashType': 'sha256',
                    'minSdkVersion': '14',
                    'nativecode': ['arm64-v8a', 'armeabi-v7a', 'x86', 'x86_64'],
                    'packageName': 'com.google.android.gms',
                    'sig': 'd91564be0d13dcff260fe019eeea8cd8',
                    'signer': '9bd06727e62796c0130eb6dab39b73157451582cbd138e86c468acc395d14165',
                    'size': 28917141,
                    'targetSdkVersion': '29',
                    'uses-permission': [
                        ['android.permission.FAKE_PACKAGE_SIGNATURE', None],
                        ['android.permission.ACCESS_NETWORK_STATE', None],
                        ['android.permission.INTERNET', None],
                        ['android.permission.ACCESS_COARSE_LOCATION', None],
                        ['android.permission.ACCESS_FINE_LOCATION', None],
                        ['android.permission.READ_PHONE_STATE', None],
                        ['android.permission.AUTHENTICATE_ACCOUNTS', None],
                        ['android.permission.GET_ACCOUNTS', None],
                        ['android.permission.MANAGE_ACCOUNTS', None],
                        ['android.permission.USE_CREDENTIALS', None],
                        ['android.permission.WAKE_LOCK', None],
                        ['android.permission.READ_SYNC_SETTINGS', None],
                        ['android.permission.WRITE_SYNC_SETTINGS', None],
                        ['android.permission.READ_SYNC_STATS', None],
                        ['android.permission.WRITE_EXTERNAL_STORAGE', None],
                        ['android.permission.READ_EXTERNAL_STORAGE', None],
                        ['com.google.android.c2dm.permission.RECEIVE', None],
                        ['com.google.android.c2dm.permission.SEND', None],
                        ['com.google.android.gtalkservice.permission.GTALK_SERVICE', None],
                        ['org.microg.gms.STATUS_BROADCAST', None],
                        ['android.permission.REQUEST_IGNORE_BATTERY_OPTIMIZATIONS', None],
                        ['android.permission.RECEIVE_BOOT_COMPLETED', None],
                        ['android.permission.CHANGE_DEVICE_IDLE_TEMP_WHITELIST', None],
                        ['android.permission.INSTALL_LOCATION_PROVIDER', None],
                        ['android.permission.ACCESS_COARSE_UPDATES', None],
                        ['org.microg.permission.FORCE_COARSE_LOCATION', None],
                        ['com.android.settings.INJECT_SETTINGS', None],
                        ['android.permission.ACCESS_WIFI_STATE', None],
                    ],
                    'versionCode': 19420020,
                    'versionName': '0.2.10.19420',
                },
                {
                    'added': 1571759626000,
                    'apkName': 'com.google.android.gms-19420018.apk',
                    'hash': '2b913d8d0e088143b647b0647a9752141c03fa6c92964b486b7723836a215a7c',
                    'hashType': 'sha256',
                    'minSdkVersion': '14',
                    'nativecode': ['arm64-v8a', 'armeabi-v7a', 'x86', 'x86_64'],
                    'packageName': 'com.google.android.gms',
                    'sig': 'd91564be0d13dcff260fe019eeea8cd8',
                    'signer': '9bd06727e62796c0130eb6dab39b73157451582cbd138e86c468acc395d14165',
                    'size': 29081787,
                    'targetSdkVersion': '29',
                    'uses-permission': [
                        ['android.permission.FAKE_PACKAGE_SIGNATURE', None],
                        ['android.permission.ACCESS_NETWORK_STATE', None],
                        ['android.permission.INTERNET', None],
                        ['android.permission.ACCESS_COARSE_LOCATION', None],
                        ['android.permission.ACCESS_FINE_LOCATION', None],
                        ['android.permission.READ_PHONE_STATE', None],
                        ['android.permission.AUTHENTICATE_ACCOUNTS', None],
                        ['android.permission.GET_ACCOUNTS', None],
                        ['android.permission.MANAGE_ACCOUNTS', None],
                        ['android.permission.USE_CREDENTIALS', None],
                        ['android.permission.WAKE_LOCK', None],
                        ['android.permission.WRITE_EXTERNAL_STORAGE', None],
                        ['android.permission.READ_EXTERNAL_STORAGE', None],
                        ['com.google.android.c2dm.permission.RECEIVE', None],
                        ['com.google.android.c2dm.permission.SEND', None],
                        ['com.google.android.gtalkservice.permission.GTALK_SERVICE', None],
                        ['org.microg.gms.STATUS_BROADCAST', None],
                        ['android.permission.REQUEST_IGNORE_BATTERY_OPTIMIZATIONS', None],
                        ['android.permission.RECEIVE_BOOT_COMPLETED', None],
                        ['android.permission.CHANGE_DEVICE_IDLE_TEMP_WHITELIST', None],
                        ['android.permission.INSTALL_LOCATION_PROVIDER', None],
                        ['android.permission.ACCESS_COARSE_UPDATES', None],
                        ['org.microg.permission.FORCE_COARSE_LOCATION', None],
                        ['com.android.settings.INJECT_SETTINGS', None],
                        ['android.permission.ACCESS_WIFI_STATE', None],
                    ],
                    'versionCode': 19420018,
                    'versionName': '0.2.9.19420',
                },
                {
                    'added': 1562115607000,
                    'apkName': 'com.google.android.gms-17785016.apk',
                    'hash': '4dbcd78d1ce738a18cc4bc8de817f36609420e6c1891c6e652605617cbbf3143',
                    'hashType': 'sha256',
                    'minSdkVersion': '14',
                    'nativecode': ['arm64-v8a', 'armeabi-v7a', 'x86', 'x86_64'],
                    'packageName': 'com.google.android.gms',
                    'sig': 'd91564be0d13dcff260fe019eeea8cd8',
                    'signer': '9bd06727e62796c0130eb6dab39b73157451582cbd138e86c468acc395d14165',
                    'size': 28999110,
                    'targetSdkVersion': '28',
                    'uses-permission': [
                        ['android.permission.FAKE_PACKAGE_SIGNATURE', None],
                        ['android.permission.ACCESS_NETWORK_STATE', None],
                        ['android.permission.INTERNET', None],
                        ['android.permission.ACCESS_COARSE_LOCATION', None],
                        ['android.permission.ACCESS_FINE_LOCATION', None],
                        ['android.permission.READ_PHONE_STATE', None],
                        ['android.permission.AUTHENTICATE_ACCOUNTS', None],
                        ['android.permission.GET_ACCOUNTS', None],
                        ['android.permission.MANAGE_ACCOUNTS', None],
                        ['android.permission.USE_CREDENTIALS', None],
                        ['android.permission.WAKE_LOCK', None],
                        ['android.permission.WRITE_EXTERNAL_STORAGE', None],
                        ['android.permission.READ_EXTERNAL_STORAGE', None],
                        ['com.google.android.c2dm.permission.RECEIVE', None],
                        ['com.google.android.c2dm.permission.SEND', None],
                        ['com.google.android.gtalkservice.permission.GTALK_SERVICE', None],
                        ['org.microg.gms.STATUS_BROADCAST', None],
                        ['android.permission.REQUEST_IGNORE_BATTERY_OPTIMIZATIONS', None],
                        ['android.permission.RECEIVE_BOOT_COMPLETED', None],
                        ['android.permission.CHANGE_DEVICE_IDLE_TEMP_WHITELIST', None],
                        ['android.permission.INSTALL_LOCATION_PROVIDER', None],
                        ['android.permission.ACCESS_COARSE_UPDATES', None],
                        ['org.microg.permission.FORCE_COARSE_LOCATION', None],
                        ['com.android.settings.INJECT_SETTINGS', None],
                        ['android.permission.ACCESS_WIFI_STATE', None],
                    ],
                    'versionCode': 17785016,
                    'versionName': '0.2.8.17785-mapbox',
                },
                {
                    'added': 1559403670000,
                    'apkName': 'com.google.android.gms-17455014.apk',
                    'hash': 'f565ecbc67b0beb23b328978a3fed66cbca4d19d2403b1c4fa3cd1f6d08910d9',
                    'hashType': 'sha256',
                    'minSdkVersion': '14',
                    'nativecode': ['arm64-v8a', 'armeabi', 'armeabi-v7a', 'x86', 'x86_64'],
                    'packageName': 'com.google.android.gms',
                    'sig': 'd91564be0d13dcff260fe019eeea8cd8',
                    'signer': '9bd06727e62796c0130eb6dab39b73157451582cbd138e86c468acc395d14165',
                    'size': 11562732,
                    'targetSdkVersion': '28',
                    'uses-permission': [
                        ['android.permission.FAKE_PACKAGE_SIGNATURE', None],
                        ['android.permission.ACCESS_NETWORK_STATE', None],
                        ['android.permission.INTERNET', None],
                        ['android.permission.ACCESS_COARSE_LOCATION', None],
                        ['android.permission.ACCESS_FINE_LOCATION', None],
                        ['android.permission.READ_PHONE_STATE', None],
                        ['android.permission.AUTHENTICATE_ACCOUNTS', None],
                        ['android.permission.GET_ACCOUNTS', None],
                        ['android.permission.MANAGE_ACCOUNTS', None],
                        ['android.permission.USE_CREDENTIALS', None],
                        ['android.permission.WAKE_LOCK', None],
                        ['android.permission.WRITE_EXTERNAL_STORAGE', None],
                        ['android.permission.READ_EXTERNAL_STORAGE', None],
                        ['com.google.android.c2dm.permission.RECEIVE', None],
                        ['com.google.android.c2dm.permission.SEND', None],
                        ['com.google.android.gtalkservice.permission.GTALK_SERVICE', None],
                        ['org.microg.gms.STATUS_BROADCAST', None],
                        ['android.permission.REQUEST_IGNORE_BATTERY_OPTIMIZATIONS', None],
                        ['android.permission.RECEIVE_BOOT_COMPLETED', None],
                        ['android.permission.CHANGE_DEVICE_IDLE_TEMP_WHITELIST', None],
                        ['android.permission.INSTALL_LOCATION_PROVIDER', None],
                        ['android.permission.ACCESS_COARSE_UPDATES', None],
                        ['org.microg.permission.FORCE_COARSE_LOCATION', None],
                        ['com.android.settings.INJECT_SETTINGS', None],
                    ],
                    'versionCode': 17455014,
                    'versionName': '0.2.7.17455',
                },
                {
                    'added': 1559404694000,
                    'apkName': 'com.google.android.gms-17455014.mapbox.apk',
                    'hash': '68261a0dbb2d9c966b48d0ca189703f3ffb6d54a0b568c4eade8776e9e12df99',
                    'hashType': 'sha256',
                    'minSdkVersion': '14',
                    'nativecode': ['arm64-v8a', 'armeabi-v7a', 'x86', 'x86_64'],
                    'packageName': 'com.google.android.gms',
                    'sig': 'd91564be0d13dcff260fe019eeea8cd8',
                    'signer': '9bd06727e62796c0130eb6dab39b73157451582cbd138e86c468acc395d14165',
                    'size': 28984278,
                    'targetSdkVersion': '28',
                    'uses-permission': [
                        ['android.permission.FAKE_PACKAGE_SIGNATURE', None],
                        ['android.permission.ACCESS_NETWORK_STATE', None],
                        ['android.permission.INTERNET', None],
                        ['android.permission.ACCESS_COARSE_LOCATION', None],
                        ['android.permission.ACCESS_FINE_LOCATION', None],
                        ['android.permission.READ_PHONE_STATE', None],
                        ['android.permission.AUTHENTICATE_ACCOUNTS', None],
                        ['android.permission.GET_ACCOUNTS', None],
                        ['android.permission.MANAGE_ACCOUNTS', None],
                        ['android.permission.USE_CREDENTIALS', None],
                        ['android.permission.WAKE_LOCK', None],
                        ['android.permission.WRITE_EXTERNAL_STORAGE', None],
                        ['android.permission.READ_EXTERNAL_STORAGE', None],
                        ['com.google.android.c2dm.permission.RECEIVE', None],
                        ['com.google.android.c2dm.permission.SEND', None],
                        ['com.google.android.gtalkservice.permission.GTALK_SERVICE', None],
                        ['org.microg.gms.STATUS_BROADCAST', None],
                        ['android.permission.REQUEST_IGNORE_BATTERY_OPTIMIZATIONS', None],
                        ['android.permission.RECEIVE_BOOT_COMPLETED', None],
                        ['android.permission.CHANGE_DEVICE_IDLE_TEMP_WHITELIST', None],
                        ['android.permission.INSTALL_LOCATION_PROVIDER', None],
                        ['android.permission.ACCESS_COARSE_UPDATES', None],
                        ['org.microg.permission.FORCE_COARSE_LOCATION', None],
                        ['com.android.settings.INJECT_SETTINGS', None],
                        ['android.permission.ACCESS_WIFI_STATE', None],
                    ],
                    'versionCode': 17455014,
                    'versionName': '0.2.7.17455-mapbox',
                },
                {
                    'added': 1538004490000,
                    'apkName': 'com.google.android.gms-13280012.apk',
                    'hash': '8e97f3ea6b09a70bfdcc50a05f2611543d7060e58d35690641b4e2e7984d596d',
                    'hashType': 'sha256',
                    'minSdkVersion': '9',
                    'nativecode': ['arm64-v8a', 'armeabi', 'armeabi-v7a', 'x86', 'x86_64'],
                    'packageName': 'com.google.android.gms',
                    'sig': 'd91564be0d13dcff260fe019eeea8cd8',
                    'signer': '9bd06727e62796c0130eb6dab39b73157451582cbd138e86c468acc395d14165',
                    'size': 2697453,
                    'targetSdkVersion': '27',
                    'uses-permission': [
                        ['android.permission.FAKE_PACKAGE_SIGNATURE', None],
                        ['android.permission.ACCESS_NETWORK_STATE', None],
                        ['android.permission.INTERNET', None],
                        ['android.permission.ACCESS_COARSE_LOCATION', None],
                        ['android.permission.ACCESS_FINE_LOCATION', None],
                        ['android.permission.READ_PHONE_STATE', None],
                        ['android.permission.AUTHENTICATE_ACCOUNTS', None],
                        ['android.permission.GET_ACCOUNTS', None],
                        ['android.permission.MANAGE_ACCOUNTS', None],
                        ['android.permission.USE_CREDENTIALS', None],
                        ['android.permission.WAKE_LOCK', None],
                        ['android.permission.WRITE_EXTERNAL_STORAGE', None],
                        ['android.permission.READ_EXTERNAL_STORAGE', None],
                        ['com.google.android.c2dm.permission.RECEIVE', None],
                        ['com.google.android.c2dm.permission.SEND', None],
                        ['com.google.android.gtalkservice.permission.GTALK_SERVICE', None],
                        ['org.microg.gms.STATUS_BROADCAST', None],
                        ['android.permission.REQUEST_IGNORE_BATTERY_OPTIMIZATIONS', None],
                        ['android.permission.RECEIVE_BOOT_COMPLETED', None],
                        ['android.permission.INSTALL_LOCATION_PROVIDER', None],
                        ['android.permission.ACCESS_COARSE_UPDATES', None],
                        ['org.microg.permission.FORCE_COARSE_LOCATION', None],
                        ['com.android.settings.INJECT_SETTINGS', None],
                    ],
                    'versionCode': 13280012,
                    'versionName': '0.2.6.13280',
                },
                {
                    'added': 1535470555000,
                    'apkName': 'com.google.android.gms-12879000.apk',
                    'hash': 'da3bae8d7491950c5586e8e69b4e598bcad88472de2fddaeddeac850e3921da4',
                    'hashType': 'sha256',
                    'minSdkVersion': '9',
                    'nativecode': ['arm64-v8a', 'armeabi', 'armeabi-v7a', 'x86', 'x86_64'],
                    'packageName': 'com.google.android.gms',
                    'sig': 'd91564be0d13dcff260fe019eeea8cd8',
                    'signer': '9bd06727e62796c0130eb6dab39b73157451582cbd138e86c468acc395d14165',
                    'size': 2696213,
                    'targetSdkVersion': '27',
                    'uses-permission': [
                        ['android.permission.FAKE_PACKAGE_SIGNATURE', None],
                        ['android.permission.ACCESS_NETWORK_STATE', None],
                        ['android.permission.INTERNET', None],
                        ['android.permission.ACCESS_COARSE_LOCATION', None],
                        ['android.permission.ACCESS_FINE_LOCATION', None],
                        ['android.permission.READ_PHONE_STATE', None],
                        ['android.permission.AUTHENTICATE_ACCOUNTS', None],
                        ['android.permission.GET_ACCOUNTS', None],
                        ['android.permission.MANAGE_ACCOUNTS', None],
                        ['android.permission.USE_CREDENTIALS', None],
                        ['android.permission.WAKE_LOCK', None],
                        ['android.permission.WRITE_EXTERNAL_STORAGE', None],
                        ['android.permission.READ_EXTERNAL_STORAGE', None],
                        ['com.google.android.c2dm.permission.RECEIVE', None],
                        ['com.google.android.c2dm.permission.SEND', None],
                        ['com.google.android.gtalkservice.permission.GTALK_SERVICE', None],
                        ['org.microg.gms.STATUS_BROADCAST', None],
                        ['android.permission.REQUEST_IGNORE_BATTERY_OPTIMIZATIONS', None],
                        ['android.permission.RECEIVE_BOOT_COMPLETED', None],
                        ['android.permission.INSTALL_LOCATION_PROVIDER', None],
                        ['android.permission.ACCESS_COARSE_UPDATES', None],
                        ['org.microg.permission.FORCE_COARSE_LOCATION', None],
                        ['com.android.settings.INJECT_SETTINGS', None],
                    ],
                    'versionCode': 12879000,
                    'versionName': '0.2.5.12879',
                },
                {
                    'added': 1468858428000,
                    'apkName': 'com.google.android.gms-9258259.apk',
                    'hash': '0b22b4dc844d2216b57d490d812d5447d50e9d141110c78dd18f11fe7ef41859',
                    'hashType': 'sha256',
                    'minSdkVersion': '14',
                    'nativecode': ['arm64-v8a', 'armeabi', 'armeabi-v7a', 'x86'],
                    'packageName': 'com.google.android.gms',
                    'sig': 'd91564be0d13dcff260fe019eeea8cd8',
                    'signer': '9bd06727e62796c0130eb6dab39b73157451582cbd138e86c468acc395d14165',
                    'size': 2220046,
                    'targetSdkVersion': '23',
                    'uses-permission': [
                        ['android.permission.FAKE_PACKAGE_SIGNATURE', None],
                        ['android.permission.ACCESS_NETWORK_STATE', None],
                        ['android.permission.INTERNET', None],
                        ['android.permission.ACCESS_COARSE_LOCATION', None],
                        ['android.permission.ACCESS_FINE_LOCATION', None],
                        ['android.permission.READ_PHONE_STATE', None],
                        ['android.permission.AUTHENTICATE_ACCOUNTS', None],
                        ['android.permission.GET_ACCOUNTS', None],
                        ['android.permission.MANAGE_ACCOUNTS', None],
                        ['android.permission.USE_CREDENTIALS', None],
                        ['android.permission.WAKE_LOCK', None],
                        ['android.permission.WRITE_EXTERNAL_STORAGE', None],
                        ['android.permission.READ_EXTERNAL_STORAGE', None],
                        ['com.google.android.c2dm.permission.RECEIVE', None],
                        ['com.google.android.c2dm.permission.SEND', None],
                        ['org.microg.gms.STATUS_BROADCAST', None],
                        ['android.permission.INSTALL_LOCATION_PROVIDER', None],
                        ['android.permission.ACCESS_COARSE_UPDATES', None],
                        ['android.permission.RECEIVE_BOOT_COMPLETED', None],
                        ['org.microg.permission.FORCE_COARSE_LOCATION', None],
                        ['com.android.settings.INJECT_SETTINGS', None],
                    ],
                    'versionCode': 9258259,
                    'versionName': '0.2.4',
                },
            ],
            'com.google.android.gsf': [
                {
                    'added': 1454843696000,
                    'apkName': 'com.google.android.gsf-8.apk',
                    'hash': '86891b174301f06a1c84187b545a0a2a57044c6b768f3e84e865908743349692',
                    'hashType': 'sha256',
                    'minSdkVersion': '10',
                    'packageName': 'com.google.android.gsf',
                    'sig': 'ca625df9dd76b6002de9c94d216cf09b',
                    'signer': '9bd06727e62796c0130eb6dab39b73157451582cbd138e86c468acc395d14165',
                    'size': 21872,
                    'targetSdkVersion': '23',
                    'uses-permission': [
                        ['com.google.android.c2dm.permission.RECEIVE', None],
                        ['com.google.android.c2dm.permission.SEND', None],
                    ],
                    'versionCode': 8,
                    'versionName': 'v0.1.0',
                }
            ],
            'org.microg.gms.droidguard': [
                {
                    'added': 1495244777000,
                    'apkName': 'org.microg.gms.droidguard-14.apk',
                    'hash': 'c7196ef8125b915f1b7c56bcd44552e88c7a23fdd47cfd87116fca1ca99aa55f',
                    'hashType': 'sha256',
                    'minSdkVersion': '9',
                    'packageName': 'org.microg.gms.droidguard',
                    'sig': 'ca625df9dd76b6002de9c94d216cf09b',
                    'signer': '9bd06727e62796c0130eb6dab39b73157451582cbd138e86c468acc395d14165',
                    'size': 107340,
                    'targetSdkVersion': '23',
                    'uses-permission': [['android.permission.INTERNET', None]],
                    'versionCode': 14,
                    'versionName': '0.1.0-10-gf64bf69',
                },
                {
                    'added': 1480510433000,
                    'apkName': 'org.microg.gms.droidguard-8.apk',
                    'hash': '5db8c7e0112a61a6be20152beaab8c67a11f397af89fc4fd1bb0219f84ac70c6',
                    'hashType': 'sha256',
                    'minSdkVersion': '9',
                    'packageName': 'org.microg.gms.droidguard',
                    'sig': 'ca625df9dd76b6002de9c94d216cf09b',
                    'signer': '9bd06727e62796c0130eb6dab39b73157451582cbd138e86c468acc395d14165',
                    'size': 107116,
                    'targetSdkVersion': '24',
                    'uses-permission': [['android.permission.INTERNET', None]],
                    'versionCode': 8,
                    'versionName': '0.1.0-4-g0ca6fb2',
                },
                {
                    'added': 1474848900000,
                    'apkName': 'org.microg.gms.droidguard-7.apk',
                    'hash': '09052279ab1b89a8350f2e003657fbc8e14b55cac0ae82caca71c3797ba7d66b',
                    'hashType': 'sha256',
                    'minSdkVersion': '19',
                    'nativecode': ['arm64-v8a', 'armeabi', 'armeabi-v7a', 'mips', 'mips64', 'x86', 'x86_64'],
                    'packageName': 'org.microg.gms.droidguard',
                    'sig': 'ca625df9dd76b6002de9c94d216cf09b',
                    'signer': '9bd06727e62796c0130eb6dab39b73157451582cbd138e86c468acc395d14165',
                    'size': 156335,
                    'targetSdkVersion': '24',
                    'uses-permission': [['android.permission.INTERNET', None]],
                    'versionCode': 7,
                    'versionName': '0.1.0-3-gbb232f2',
                },
                {
                    'added': 1474819186000,
                    'apkName': 'org.microg.gms.droidguard-6.apk',
                    'hash': '73da7e80004fc24c031d038fe2ece12a611bf681e7b9f1997a190463d2003ded',
                    'hashType': 'sha256',
                    'minSdkVersion': '9',
                    'nativecode': ['arm64-v8a', 'armeabi', 'armeabi-v7a', 'mips', 'mips64', 'x86', 'x86_64'],
                    'packageName': 'org.microg.gms.droidguard',
                    'sig': 'ca625df9dd76b6002de9c94d216cf09b',
                    'signer': '9bd06727e62796c0130eb6dab39b73157451582cbd138e86c468acc395d14165',
                    'size': 155975,
                    'targetSdkVersion': '24',
                    'uses-permission': [['android.permission.INTERNET', None]],
                    'versionCode': 6,
                    'versionName': '0.1.0-2-g03d6444',
                },
                {
                    'added': 1474816326000,
                    'apkName': 'org.microg.gms.droidguard-5.apk',
                    'hash': '3be2ae784d21592bfbe2a6208dd8d0f881afbaae822705a800d0011df9f260a8',
                    'hashType': 'sha256',
                    'minSdkVersion': '9',
                    'nativecode': ['arm64-v8a', 'armeabi', 'armeabi-v7a', 'mips', 'mips64', 'x86', 'x86_64'],
                    'packageName': 'org.microg.gms.droidguard',
                    'sig': 'ca625df9dd76b6002de9c94d216cf09b',
                    'signer': '9bd06727e62796c0130eb6dab39b73157451582cbd138e86c468acc395d14165',
                    'size': 155855,
                    'targetSdkVersion': '24',
                    'uses-permission': [['android.permission.INTERNET', None]],
                    'versionCode': 5,
                    'versionName': '0.1.0-1-geccdb6b',
                },
                {
                    'added': 1474766212000,
                    'apkName': 'org.microg.gms.droidguard-4.apk',
                    'hash': 'b6fcc32a39218d72876f274ba7ddcfa12b7c43ba62f08b40fc445f8f5e597396',
                    'hashType': 'sha256',
                    'minSdkVersion': '19',
                    'nativecode': ['arm64-v8a', 'armeabi', 'armeabi-v7a'],
                    'packageName': 'org.microg.gms.droidguard',
                    'sig': 'ca625df9dd76b6002de9c94d216cf09b',
                    'signer': '9bd06727e62796c0130eb6dab39b73157451582cbd138e86c468acc395d14165',
                    'size': 142844,
                    'targetSdkVersion': '23',
                    'uses-permission': [['android.permission.INTERNET', None]],
                    'versionCode': 4,
                    'versionName': '0.1.0',
                },
            ],
        },
    }

    def setUp(self):
        repo.Repo.timeout = 0.1
        self.r = repo.Repo(self.url, headers={'User-Agent': 'unittest'})
        self.r.log = self.log

    def test_init(self):
        self.assertEqual(self.r in self.r.instances, True)
        self.assertEqual(self.r.url, self.url)
        self.assertEqual(self.r.fingerprint, None)
        self.assertEqual(self.r.timeout, 0.1)
        self.assertEqual(self.r.main_mirror.url, self.url)
        self.assertEqual(self.r.integrated_mirrors, set())
        self.assertEqual(self.r.additional_mirrors, set())
        self.assertEqual(self.r.additional_mirrors, set())
        self.assertEqual(self.r.hostname, 'main.local')
        self.assertEqual(self.r.protocol, 'https')
        self.assertEqual(self.r.onion, False)
        self.assertEqual(self.r.headers, {'User-Agent': 'unittest'})
        self.assertEqual(self.r.errors, {})

    @responses.activate
    def test_get_index_version_404(self):
        responses.add(responses.HEAD, self.url + 'index-v1.jar', status=404)
        responses.add(responses.HEAD, self.url + 'index.jar', status=404)

        self.assertEqual(self.r.get_index_version(), False)
        self.assertEqual(self.r.index_file, None)
        self.assertEqual(self.r.errors, {'index_version': 'Could not find an index file'})

    @responses.activate
    def test_get_index_version_basic(self):
        responses.add(responses.HEAD, self.url + 'index-v1.jar', status=404)
        responses.add(responses.HEAD, self.url + 'index.jar', status=200)

        self.r.get_index_version()
        self.assertEqual(self.r.index_file, 'index.jar')

        responses.replace(responses.HEAD, self.url + 'index-v1.jar', status=200)
        self.assertEqual(self.r.get_index_version(), True)
        self.assertEqual(self.r.index_file, 'index-v1.jar')
        self.assertEqual(self.r.errors, {})

    @responses.activate
    def test_get_index_version_mirror(self):
        # main mirror down:
        responses.add(responses.HEAD, self.url + 'index-v1.jar', status=504)
        responses.add(responses.HEAD, self.url + 'index.jar', status=504)
        responses.add(responses.HEAD, self.integrated_mirror_url + 'index-v1.jar', status=404)
        responses.add(responses.HEAD, self.integrated_mirror_url + 'index.jar', status=200)

        self.r.integrated_mirrors = {mirror.Mirror(self.integrated_mirror_url)}
        self.r.additional_mirrors = {mirror.Mirror(self.additional_mirror_url)}
        self.assertEqual(self.r.get_index_version(), True)
        self.assertEqual(self.r.index_file, 'index.jar')
        self.assertEqual(self.r.errors, {})

    @responses.activate
    def test_get_index_version_504(self):
        # main mirror down:
        responses.add(responses.HEAD, self.url + 'index-v1.jar', status=504)
        responses.add(responses.HEAD, self.url + 'index.jar', status=504)

        self.assertEqual(self.r.get_index_version(), False)
        self.assertEqual(self.r.index_file, None)
        err = {
            'index_version': 'Could not get index version: 504 Server Error: Gateway Timeout for url: %sindex.jar; 504 Server Error: Gateway Timeout for url: %sindex-v1.jar'
            % (self.url, self.url)
        }
        self.assertEqual(self.r.errors, err)

    @responses.activate
    def test_get_index_mirrors(self):
        with open('tests/test_files/index-v1.jar', 'rb') as fp:
            body = fp.read()

        responses.add(responses.GET, self.url + 'index-v1.jar', status=200, body=body)

        self.r.index_file = 'index-v1.jar'
        self.r.fingerprint = '9BD06727E62796C0130EB6DAB39B73157451582CBD138E86C468ACC395D14165'
        self.assertEqual(self.r.get_index(), True)
        self.assertEqual(self.r.index['repo']['address'], 'https://microg.org/fdroid/repo')
        self.assertEqual(self.r.index, self.index)

        # main mirror down:
        responses.replace(responses.GET, self.url + 'index-v1.jar', status=504, body='')
        responses.add(responses.GET, self.additional_mirror_url + 'index-v1.jar', status=200, body=body)
        self.r.additional_mirrors = {mirror.Mirror(self.additional_mirror_url)}
        self.assertEqual(self.r.get_index(), True)
        self.assertEqual(self.r.index['repo']['address'], 'https://microg.org/fdroid/repo')
        self.assertEqual(self.r.index, self.index)
        self.assertEqual(self.r.errors, {})

        # same with integrated_mirrors instead of additional_mirrors
        responses.replace(responses.GET, self.url + 'index-v1.jar', status=504, body='')
        responses.add(responses.GET, self.integrated_mirror_url + 'index-v1.jar', status=200, body=body)
        self.r.integrated_mirrors = {mirror.Mirror(self.integrated_mirror_url)}
        self.assertEqual(self.r.get_index(), True)
        self.assertEqual(self.r.index['repo']['address'], 'https://microg.org/fdroid/repo')
        self.assertEqual(self.r.index, self.index)
        self.assertEqual(self.r.errors, {})

    @responses.activate
    def test_get_index_legacy(self):
        self.r.index_file = 'index.jar'
        self.assertEqual(self.r.get_index(), False)
        self.assertEqual(self.r.index, None)
        self.assertEqual(self.r.errors, {'get_index': 'Legacy index not supported (yet)'})

    @responses.activate
    def test_get_index_fingerprint_wrong(self):
        with open('tests/test_files/index-v1.jar', 'rb') as fp:
            body = fp.read()

        responses.add(responses.GET, self.url + 'index-v1.jar', status=200, body=body)

        self.r.index_file = 'index-v1.jar'
        self.r.fingerprint = 'DEADC0DE'
        self.assertEqual(self.r.get_index(), False)
        self.assertEqual(self.r.index, None)
        self.assertEqual(self.r.fingerprint, None)
        self.assertEqual(self.r.errors, {'get_index': 'Could not verify fingerprint DEADC0DE'})
        self.assertEqual(self.r.index, None)

    @responses.activate
    def test_get_index_no_fingerprint(self):
        with open('tests/test_files/index-v1.jar', 'rb') as fp:
            body = fp.read()

        responses.add(responses.GET, self.url + 'index-v1.jar', status=200, body=body)

        self.r.index_file = 'index-v1.jar'
        self.assertEqual(self.r.get_index(), True)
        self.assertEqual(self.r.index['repo']['address'], 'https://microg.org/fdroid/repo')
        self.assertEqual(self.r.index, self.index)
        self.assertEqual(self.r.fingerprint, None)
        self.assertEqual(self.r.errors, {})

    @responses.activate
    def test_get_index_504(self):
        with open('tests/test_files/index-v1.jar', 'rb') as fp:
            body = fp.read()

        responses.add(responses.GET, self.url + 'index-v1.jar', status=504, body=body)

        self.r.index_file = 'index-v1.jar'
        self.assertEqual(self.r.get_index(), False)
        self.assertEqual(self.r.index, None)
        self.assertEqual(self.r.fingerprint, None)
        self.assertEqual(self.r.errors, {'get_index': 'Could not download index for %s' % self.url})

    @responses.activate
    def test_get_infos_from_index(self):
        self.r.index = self.index
        self.assertEqual(self.r.fingerprint, None)
        self.r.get_infos_from_index()
        self.assertEqual(
            self.r.errors, {'address': 'missmatch https://main.local/repo/, https://microg.org/fdroid/repo'}
        )
        self.assertEqual(self.r.fingerprint, '9BD06727E62796C0130EB6DAB39B73157451582CBD138E86C468ACC395D14165')
        repo = self.index['repo']
        self.assertEqual(self.r.description, repo['description'])
        self.assertEqual(self.r.icon, repo['icon'])
        self.assertEqual(self.r.name, repo['name'])
        self.assertEqual(self.r.pubkey, repo['pubkey'])
        self.assertEqual(self.r.timestamp, repo['timestamp'])
        self.assertEqual(self.r.version, repo['version'])
        self.assertEqual(self.r.integrated_mirrors, set())
        self.assertRaises(AttributeError, lambda: self.r.maxage)

    @responses.activate
    def test_get_infos_from_index_wrong_fingerprint(self):
        self.r.index = self.index
        self.r.url = 'https://microg.org/fdroid/repo/'
        self.r.fingerprint = 'DEADC0DE'
        self.r.get_infos_from_index()
        self.assertEqual(
            self.r.errors,
            {'fingerprint': 'missmatch DEADC0DE, 9BD06727E62796C0130EB6DAB39B73157451582CBD138E86C468ACC395D14165'},
        )
        self.assertEqual(self.r.fingerprint, '9BD06727E62796C0130EB6DAB39B73157451582CBD138E86C468ACC395D14165')
        repo = self.index['repo']
        self.assertEqual(self.r.description, repo['description'])
        self.assertEqual(self.r.icon, repo['icon'])
        self.assertEqual(self.r.name, repo['name'])
        self.assertEqual(self.r.pubkey, repo['pubkey'])
        self.assertEqual(self.r.timestamp, repo['timestamp'])
        self.assertEqual(self.r.version, repo['version'])
        self.assertEqual(self.r.integrated_mirrors, set())
        self.assertRaises(AttributeError, lambda: self.r.maxage)

    # TODO
    @unittest.skip
    @responses.activate
    def test_get_additional_mirrors(self):
        readme = '''
# Mirror Monitor

Monitor the status of known mirrors F-Droid Mirrors


## Active Mirrors of https://f-droid.org/

### repo + archive
* %s
* %s
* https://f-droid.org/
* rsync://fdroid-mirror@mirror.f-droid.org/ RSYNC_PASSWORD=dont-abuse-me-please
* https://fdroid.tetaneutral.net/fdroid/

### repo
* https://mirror.cyberbits.eu/fdroid/
* rsync://mirror.cyberbits.eu/fdroid/
''' % (
            self.url,
            self.integrated_mirror_url,
        )

        tmp = tempfile.NamedTemporaryFile()
        with open(tmp.name, 'wt') as f:
            f.write(readme)
        self.r.integrated_mirrors.add(mirror.Mirror(self.integrated_mirror_url))
        self.r.get_additional_mirrors(filepath=tmp.name)

        self.assertEqual(len(self.r.additional_mirrors), 5)

        m1 = 'https://f-droid.org/'
        m2 = 'rsync://fdroid-mirror@mirror.f-droid.org/'
        m3 = 'https://fdroid.tetaneutral.net/fdroid/'
        m4 = 'https://mirror.cyberbits.eu/fdroid/'
        m5 = 'rsync://mirror.cyberbits.eu/fdroid/'

        m = [m1, m2, m3, m4, m5]
        urls = []

        for add_mirror in self.r.additional_mirrors:
            urls.append(add_mirror.url)
            if add_mirror.url == m2:
                self.assertEqual(add_mirror.rsync_pw, 'dont-abuse-me-please')
            else:
                self.assertEqual(add_mirror.rsync_pw, None)

        self.assertCountEqual(urls, m)

    @responses.activate
    def test_get_all_success(self):
        with open('tests/test_files/index-v1.jar', 'rb') as fp:
            body = fp.read()

        responses.add(responses.HEAD, self.url + 'index-v1.jar', status=200)
        responses.add(responses.GET, self.url + 'index-v1.jar', status=200, body=body)

        success = self.r.get_all()
        self.assertEqual(success, True)

    @responses.activate
    def test_get_all_version_fail(self):
        with open('tests/test_files/index-v1.jar', 'rb') as fp:
            body = fp.read()

        responses.add(responses.HEAD, self.url + 'index-v1.jar', status=504)
        responses.add(responses.HEAD, self.url + 'index.jar', status=504)
        responses.add(responses.GET, self.url + 'index-v1.jar', status=200, body=body)

        success = self.r.get_all(tries=2)
        self.assertEqual(success, False)

    @responses.activate
    def test_get_all_index_fail(self):
        responses.add(responses.HEAD, self.url + 'index-v1.jar', status=200)
        responses.add(responses.GET, self.url + 'index-v1.jar', status=504)
        responses.add(responses.HEAD, self.url + 'index.jar', status=504)

        success = self.r.get_all(tries=2)
        self.assertEqual(success, False)


if __name__ == '__main__':
    unittest.main()
